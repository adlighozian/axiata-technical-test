# Axiata Technical Test

Welcome to the Axiata Technical Test project! This repository contains the implementation of the technical test for Axiata.

## Table of Contents

- [Introduction](#introduction)
- [Requirements](#requirements)
- [Installation](#installation)
- [Usage](#usage)
- [API Documentation](#api-documentation)
- [License](#license)

## Introduction

This project is developed as part of a technical test for Axiata. It includes a RESTful API built with Go, designed to handle various operations. The API allows you to manage posts with functionalities such as creating, reading, updating, and deleting posts.

## Requirements

- Go 1.16 or higher
- PostgreSQL
- Git

## Installation

To install and run this project locally, follow these steps:

1. Clone the repository:

   ```sh
   git clone https://gitlab.com/adlighozian/axiata-technical-test.git
   cd axiata-technical-test
   ```

2. Set up the database and environment variables. Create a `.env` file in the project root and add your database configuration:

   ```env
    DB_CONNECTION_PG = "postgres://postgres:admin@localhost:5432/go-axiata?sslmode=disable"

    PORT = "9000"
   ```

3. Install the dependencies:

   ```sh
   go mod download

   go mod tidy
   ```

4. Set up PostgreSQL database:

   ```sql
    CREATE TABLE posts (
        id UUID DEFAULT gen_random_uuid() PRIMARY KEY,
        title VARCHAR(255) NOT NULL,
        content TEXT NOT NULL,
        status VARCHAR(50) NOT NULL CHECK (status IN ('draft', 'publish')),
        publish_date DATE,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    );

    CREATE TABLE tags (
        id UUID DEFAULT gen_random_uuid() PRIMARY KEY,
        label VARCHAR(100) NOT NULL UNIQUE
    );

    CREATE TABLE post_tags (
        id UUID DEFAULT gen_random_uuid() PRIMARY KEY,
        post_id UUID NOT NULL,
        tag_id UUID NOT NULL,
        FOREIGN KEY (post_id) REFERENCES posts (id) ON DELETE CASCADE,
        FOREIGN KEY (tag_id) REFERENCES tags (id) ON DELETE CASCADE
    );

    CREATE TABLE users (
         id UUID DEFAULT gen_random_uuid() PRIMARY key,
         username VARCHAR(50) UNIQUE NOT NULL,
         password VARCHAR(100) NOT NULL,
         role VARCHAR(50) NOT NULL
    );

   ```

5. Start the application:

   ```sh
        go run main.go
   ```

## Usage

After starting the application, the API will be available at `http://localhost:9000`. You can interact with the API using tools like Postman or cURL.

## API Documentation

The API documentation is available via Postman and can be accessed using the following link: [API Documentation](https://documenter.getpostman.com/view/28298509/2sA3QmEupM)

## Endpoints

### Posts

- `GET /posts`: Retrieve a list of posts.
- `GET /posts/:id`: Retrieve a single post by ID.
- `POST /posts`: Create a new post.
- `PUT /posts/:id`: Update an existing post by ID.
- `DELETE /posts/:id`: Delete a post by ID.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.
